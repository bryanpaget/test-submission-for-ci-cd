#!/usr/bin/env python


class Submission():

    def __init__():
        pass

    def _cleaning():
        print("Performing Data Ingestion and Cleaning...")

    def _visualization():
        print("Performing Data Visualization...")

    def _predictions():
        print("Performing Predictions...")

    def _submit():
        print("Submitting Work...")


if __name__ == "__main__":

    submission = Submission()

    submission._cleaning()
    submission._predictions()
    submission._predictions()
    submission._submit()